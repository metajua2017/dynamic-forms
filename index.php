<?php

	require_once 'Medoo.php';
	
	$db = new Medoo( [
		'database_type' => 'mysql',
		'database_name' => 'metajua',
		'server' => 'localhost',
		'username' => 'root',
		'password' => '',
		'charset' => 'utf8'
	] );
	
	$components = $db->select( 'form_components', [ 'id', 'type', 'label', 'instructions', 'order' ] );
	
	foreach( $components as $component ) :
	
		$component = ( object ) $component;
	
		echo '<div>
			<label for="' . $component->id . '">' . $component->label . '</label>';
	
			if ( $component->type == 1 ) :
		
				echo '<input type="text" />';
				
			elseif ( $component->type == 5 ) :
			
				$component_meta = $db->select( 'form_components_meta', [ 'key', 'value' ], [ 'form_component' => $component->id ] );
				
				$values = explode( ',', $component_meta[ 0 ][ 'value' ] );
				$titles = explode( ',', $component_meta[ 1 ][ 'value' ] );
				
				foreach( $values as $index => $value ) :
				
					echo '<input type="radio" name="' . $component->id . '" value="' . $value . '" /> ' . $titles[ $index ] . '<br />';
				
				endforeach;
			
			
			elseif ( $component->type == 4 ) :
			
				echo '<textarea></textarea>';
			
			endif;
			
		echo '</div>';
	
	endforeach;


?>